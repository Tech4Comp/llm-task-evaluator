# Importing the necessary modules and functions
from llama_index import SimpleDirectoryReader, ServiceContext, VectorStoreIndex, load_index_from_storage, set_global_service_context, LLMPredictor
from llama_index.llms import OpenAI
from llama_index.tools import QueryEngineTool, ToolMetadata
from llama_index.query_engine import SubQuestionQueryEngine
from llama_index.question_gen.llm_generators import LLMQuestionGenerator
from llama_index import StorageContext
from llama_index.callbacks import CallbackManager, TokenCountingHandler
from llama_index.indices.postprocessor import SimilarityPostprocessor
from llama_index.response_synthesizers import get_response_synthesizer
from llama_index.agent import ReActAgent
from task_assessor_agent_utils import CustomOutputParser, ReActChatFormatter, CustomPrompts, Costs, agent_input_instructions

import tiktoken
import logging
import sys
import openai
import glob
import chainlit as cl
from chainlit import user_session

# Set up logging
logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logging.getLogger().addHandler(logging.StreamHandler(stream=sys.stdout))
logging.DEBUG = True

# Variables to control cost printing and evaluation
print_cost = True

# Get a list of all PDF files or storage path if they are already indexed
lectures = glob.glob("sources/main_books/book_1.pdf")
storage_dir = "./storage/data/main_books/book_1"


## Function to load the index from storage or create a new one
@cl.cache ## Allow to cache the function
def load_context():
    # Load or generate the index for all lectures
    try:
        # Rebuild the storage context
        context = StorageContext.from_defaults(persist_dir=storage_dir)
        # Load the index
        index = load_index_from_storage(context)
    except:
        # Storage not found; create a new one
        docs = SimpleDirectoryReader(input_files=lectures).load_data()
        print(docs[0])
        index = VectorStoreIndex.from_documents(docs)
        index.storage_context.persist(storage_dir)

    return index


@cl.on_chat_start
async def factory():
    # Set up OpenAI API client
    user_env = user_session.get("env")
    openai.api_key = user_env.get("OPENAI_API_KEY")

    # Set up the service context
    # OpenAI is set as the language model with specified parameters
    llm = OpenAI(temperature=0, model="gpt-3.5-turbo", top_p=1)

    # LLM predictor is set up using the OpenAI language model
    llm_predictor = LLMPredictor(llm=llm)

    # Set up a token counter using the tiktoken tokenizer
    token_counter = TokenCountingHandler(tokenizer=tiktoken.encoding_for_model("gpt-3.5-turbo").encode, verbose=True)

    # Set up a callback manager
    callback_manager = CallbackManager([cl.LlamaIndexCallbackHandler(), token_counter])

    # Define service context and set it as the global default
    service_context = ServiceContext.from_defaults(llm_predictor=llm_predictor, callback_manager=callback_manager)
    set_global_service_context(service_context)

    # Object to calculate costs
    costs = Costs(token_counter)
    cl.user_session.set("costs", costs)

    # Main script execution
    one_index = load_context()
    # Get custom prompts for the following query engines
    custom_prompts = CustomPrompts()

    single_query_engine = one_index.as_query_engine(service_context=service_context, similarity_top_k=3,
                                                    node_postprocessors=[
                                                        SimilarityPostprocessor(similarity_cutoff=0.7)
                                                    ], text_qa_template=custom_prompts.get_text_qa_template())

    # Create QueryEngineTool with the engine and metadata
    single_query_engine_tool = QueryEngineTool(
        query_engine=single_query_engine,
        metadata=ToolMetadata(name="Lecture Source",
                              description="Useful when you want to answer queries that require analyzing lecture content.")
    )

    # Define the list of query engine tools
    query_engine_tools = [single_query_engine_tool]

    question_gen = LLMQuestionGenerator.from_defaults(
        service_context=service_context, prompt_template_str=custom_prompts.get_sub_question_query_engine_prompt()
    )

    response_synthesizer = get_response_synthesizer(text_qa_template=custom_prompts.get_text_qa_template())

    s_engine = SubQuestionQueryEngine.from_defaults(
        query_engine_tools=query_engine_tools, question_gen=question_gen, service_context=service_context, verbose=True,
        response_synthesizer=response_synthesizer
    )

    s_engine_tool = QueryEngineTool(
        query_engine=s_engine,
        metadata=ToolMetadata(name="Lecture Source",
                              description="Useful when you want to answer queries that require analyzing lecture content. "
                                          "It is a must to always use this tool. "
                                          "Use a detailed plain text question as input to the tool.")
    )

    output_parser = CustomOutputParser()

    react_chat_formatter = ReActChatFormatter(tools=[s_engine_tool])

    agent_chain = ReActAgent.from_tools([s_engine_tool], llm=llm, verbose=True, callback_manager=callback_manager,
                                        output_parser=output_parser, react_chat_formatter=react_chat_formatter)

    cl.user_session.set("chat_agent_engine", agent_chain)


# Function to handle messages in the chat
@cl.on_message
async def send_message(message):
    query_engine = cl.user_session.get("chat_agent_engine")

    while len(query_engine.chat_history) > 5:
        query_engine.chat_history.pop(0)

    response = await cl.make_async(query_engine.chat)(message + agent_input_instructions)

    costs = cl.user_session.get("costs")

    agent_cost = costs.calculate_agent_cost(query_engine, response)

    inner_steps_cost = costs.get_costs()

    print("LLM Cost: $", str(round(agent_cost + inner_steps_cost, 5)))

    if print_cost:
        elements = [cl.Text(content="$" + str(round(agent_cost + inner_steps_cost, 5)), name="LLM Cost", display="inline")]

        # Send the response
        await cl.Message(content=response.response, elements=elements).send()
    else:
        # Send the response
        await cl.Message(content=response.response).send()


