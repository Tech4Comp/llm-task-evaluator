Die Aufgabe muss den Studierenden dazu bringen, sein Wissen über Beziehungen zwischen den Grundelementen innerhalb einer größeren Struktur, die jene funktionstüchtig machen, zu nutzen.
Dabei solltest du eine der folgenden Kategorien auswählen:
1.	„Kenntnis der Klassifikation und Kategorisierung“
a.	Beispiele: Hierarchie algebraischer Grundstrukturen/gängiger Zahlbereiche (Gruppen, Ringe, Körper), Klassifikation von Funktionen (lineare/nichtlineare; stetige/differenzierbare/integrierbare; umkehrbar), Klassifikation von Mengen (endlich, abzählbar, überabzählbar), Klassifikation von Differentialgleichungen (gewöhnlich, partiell; linear/nichtlinear), Hierarchie von Räumen (topologische/ metrische/normierte/Innenprodukt; Banach-/Hilbert-)
2.	„Kenntnis der Prinzipien und Generalisierungen“
a.	Beispiele: Beweisprinzip der vollständigen Induktion, Vervollständigung, Zwischenwertsatz, Hauptsatz der Differential- und Integralrechnung, Gesetz der großen Zahlen
3.	„Kenntnis der Theorien, Modelle und Strukturen“
a.	Beispiele: auf Axiomen basierende mathematische Theorien (Aussagenlogik und Mengenlehre), Galoistheorie, Lineare Struktur von Funktionenräumen
