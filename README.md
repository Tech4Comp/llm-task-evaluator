# llm-task-generator

## Description
This is a Python Streamlit app for generating competency-based and concept-based math tasks

## Getting started

1. Clone the repository:
```
git clone https://github.com/Tech4Comp/llm-task-generator.git
```
2. Install the dependencies:
```
pip install -r requirements.txt
```
3. Run the app:
```
streamlit run .\agent-generator.py
```
