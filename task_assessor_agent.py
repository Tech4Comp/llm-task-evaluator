# Importing the necessary modules and functions
from llama_index import SimpleDirectoryReader, ServiceContext, VectorStoreIndex, load_index_from_storage, set_global_service_context, LLMPredictor
from llama_index.llms import OpenAI
from llama_index.tools import QueryEngineTool, ToolMetadata
from llama_index.query_engine import SubQuestionQueryEngine
from llama_index.question_gen.llm_generators import LLMQuestionGenerator
from llama_index import StorageContext
from llama_index.callbacks import CallbackManager, TokenCountingHandler
from llama_index.indices.postprocessor import SimilarityPostprocessor
from llama_index.response_synthesizers import get_response_synthesizer
from llama_index.agent import ReActAgent
from llama_index.response_synthesizers import ResponseMode
from llama_index.node_parser import SimpleNodeParser
from task_assessor_agent_utils import CustomOutputParser, ReActChatFormatter, CustomPrompts, Costs, Tasks, agent_input_instructions

import tiktoken
import logging
import sys
import openai
import glob
import chainlit as cl
from pylatexenc.latex2text import LatexNodes2Text
from chainlit import user_session

# Set up logging
logging.basicConfig(stream=sys.stdout, level=logging.INFO)
logging.getLogger().addHandler(logging.StreamHandler(stream=sys.stdout))
logging.DEBUG = True

# Variables to control cost printing and evaluation
print_cost = True

# Get a list of all PDF files or storage path if they are already indexed
lectures = glob.glob("sources/main_books/book_1.pdf")
lectures_storage_dir = "./storage/data/main_books/book_1"

# Get a list of all PDF files or storage path if they are already indexed
tasks_questions = glob.glob("data/tasks_questions/train_set_questions.pdf")
tasks_questions_storage_dir = "./storage/data/tasks_questions"

# Get a list of all PDF files or storage path if they are already indexed
tasks_answers = glob.glob("data/tasks_answers/train_set_answers.pdf")
tasks_answers_storage_dir = "./storage/data/tasks_answers"

# Get a list of all PDF files or storage path if they are already indexed
tasks_feedback = glob.glob("data/tasks_feedback/train_set_feedback.pdf")
tasks_feedback_storage_dir = "./storage/data/tasks_feedback"

tasks = Tasks()
tasks.format_tasks("data/train_set.json", "data/train_set_feedback.txt")


## Function to load the index from storage or create a new one
@cl.cache ## Allow to cache the function
def load_lecture_context():
    # Load or generate the index for all lectures
    try:
        # Rebuild the storage context
        context = StorageContext.from_defaults(persist_dir=lectures_storage_dir)
        # Load the index
        index = load_index_from_storage(context)
    except:
        # Storage not found; create a new one
        docs = SimpleDirectoryReader(input_files=lectures).load_data()
        index = VectorStoreIndex.from_documents(docs)
        index.storage_context.persist(lectures_storage_dir)

    return index

## Function to load the index from storage or create a new one
@cl.cache ## Allow to cache the function
def load_tasks_questions_context():
    # Load or generate the index for all lectures
    try:
        # Rebuild the storage context
        context = StorageContext.from_defaults(persist_dir=tasks_questions_storage_dir)
        # Load the index
        index = load_index_from_storage(context)
    except:
        # Storage not found; create a new one
        docs = SimpleDirectoryReader(input_files=tasks_questions).load_data()
        index = VectorStoreIndex.from_documents(docs)
        index.storage_context.persist(tasks_questions_storage_dir)

    return index

## Function to load the index from storage or create a new one
@cl.cache ## Allow to cache the function
def load_tasks_answers_context():
    # Load or generate the index for all lectures
    try:
        # Rebuild the storage context
        context = StorageContext.from_defaults(persist_dir=tasks_answers_storage_dir)
        # Load the index
        index = load_index_from_storage(context)
    except:
        # Storage not found; create a new one
        docs = SimpleDirectoryReader(input_files=tasks_answers).load_data()
        index = VectorStoreIndex.from_documents(docs)
        index.storage_context.persist(tasks_answers_storage_dir)

    return index

## Function to load the index from storage or create a new one
@cl.cache ## Allow to cache the function
def load_tasks_feedback_context():
    # Load or generate the index for all lectures
    try:
        # Rebuild the storage context
        context = StorageContext.from_defaults(persist_dir=tasks_feedback_storage_dir)
        # Load the index
        index = load_index_from_storage(context)
    except:
        # Storage not found; create a new one
        docs = SimpleDirectoryReader(input_files=tasks_feedback).load_data()
        index = VectorStoreIndex.from_documents(docs)
        index.storage_context.persist(tasks_feedback_storage_dir)

    return index


@cl.on_chat_start
async def factory():
    # Set up OpenAI API client
    user_env = user_session.get("env")
    openai.api_key = user_env.get("OPENAI_API_KEY")

    # Set up the service context
    # OpenAI is set as the language model with specified parameters
    llm = OpenAI(temperature=0, model="gpt-3.5-turbo", top_p=1)

    # LLM predictor is set up using the OpenAI language model
    llm_predictor = LLMPredictor(llm=llm)

    # Set up a token counter using the tiktoken tokenizer
    token_counter = TokenCountingHandler(tokenizer=tiktoken.encoding_for_model("gpt-3.5-turbo").encode, verbose=True)

    # Set up a callback manager
    callback_manager = CallbackManager([cl.LlamaIndexCallbackHandler(), token_counter])

    # Define service context and set it as the global default
    # Create a SimpleNodeParser instance with customized options
    node_parser = SimpleNodeParser.from_defaults(chunk_size=2048, chunk_overlap=30)
    service_context = ServiceContext.from_defaults(llm_predictor=llm_predictor, callback_manager=callback_manager, node_parser=node_parser)
    set_global_service_context(service_context)

    # Object to calculate costs
    costs = Costs(token_counter)
    cl.user_session.set("costs", costs)

    # Main script execution
    lectures_index = load_lecture_context()

    tasks_questions_index = load_tasks_questions_context()

    tasks_answers_index = load_tasks_answers_context()

    tasks_feedback_index = load_tasks_feedback_context()

    # Get custom prompts for the following query engines
    custom_prompts = CustomPrompts()

    single_query_engine = lectures_index.as_query_engine(service_context=service_context, similarity_top_k=1,
                                                         node_postprocessors=[
                                                             SimilarityPostprocessor(similarity_cutoff=0.7)
                                                         ], text_qa_template=custom_prompts.get_text_qa_template())

    # Create QueryEngineTool with the engine and metadata
    single_query_engine_tool = QueryEngineTool(
        query_engine=single_query_engine,
        metadata=ToolMetadata(name="Lecture Source",
                              description="Useful when you want to answer queries that require analyzing lecture content.")
    )

    tasks_questions_query_engine = tasks_questions_index.as_query_engine(service_context=service_context, similarity_top_k=1,
                                                    node_postprocessors=[
                                                        SimilarityPostprocessor(similarity_cutoff=0.7)
                                                    ], text_qa_template=custom_prompts.get_text_qa_tasks_questions_template(), response_mode=ResponseMode.REFINE)

    # Create QueryEngineTool with the engine and metadata
    tasks_questions_query_engine_tool = QueryEngineTool(
        query_engine=tasks_questions_query_engine,
        metadata=ToolMetadata(name="Tasks Questions",
                              description="Must be used when you the user asks to get tasks for some concept. It must be used when user wants to get a task, you must provide their complete text (task) as an input to this tool. "
                                          "Use the user's input text exactly letter by letter as the value for the 'input' key provided to the tool, in a JSON format representing the kwargs (e.g. {{'input': [user's input text]}}). "
                                          "Remember the output format previously mentioned to be used strictly. "
                                          "It is a must to always use this tool. "
                                          "Use only the output of Aufgabe (Task) from this tool exactly as your output and final response. "
                                          "Copy the output from this tool letter by letter to be exactly the same as your output (Answer:).")
    )

    tasks_answers_query_engine = tasks_answers_index.as_query_engine(service_context=service_context,
                                                                         similarity_top_k=1,
                                                                         node_postprocessors=[
                                                                             SimilarityPostprocessor(
                                                                                 similarity_cutoff=0.7)
                                                                         ],
                                                                         text_qa_template=custom_prompts.get_text_qa_tasks_answers_template(), response_mode=ResponseMode.REFINE)

    # Create QueryEngineTool with the engine and metadata
    tasks_answers_query_engine_tool = QueryEngineTool(
        query_engine=tasks_answers_query_engine,
        metadata=ToolMetadata(name="Tasks Solutions",
                              description="Must be used when you want to get answers and solutions for the tasks the user tried to solve. It must be used when user wants to get the solution for a task, you must provide their complete text (task) as an input to this tool. "
                                          "Use the user's input text exactly letter by letter as the value for the 'input' key provided to the tool, in a JSON format representing the kwargs (e.g. {{'input': [user's input text]}}). "
                                          "Remember the output format previously mentioned to be used strictly. "
                                          "It is a must to always use this tool. "
                                          "Use only the output of Lösung (Solution) from this tool exactly as your output and final response. "
                                          "Copy the output from this tool letter by letter to be exactly the same as your output (Answer:).")
    )

    tasks_feedback_query_engine = tasks_feedback_index.as_query_engine(service_context=service_context,
                                                                     similarity_top_k=2,
                                                                     node_postprocessors=[
                                                                         SimilarityPostprocessor(
                                                                             similarity_cutoff=0.7)
                                                                     ],
                                                                     text_qa_template=custom_prompts.get_text_qa_tasks_feedback_template(),
                                                                     response_mode=ResponseMode.COMPACT)

    # Create QueryEngineTool with the engine and metadata
    tasks_feedback_query_engine_tool = QueryEngineTool(
        query_engine=tasks_feedback_query_engine,
        metadata=ToolMetadata(name="Tasks Feedback",
                              description="Must be used when you want to get evaluation and feedback for the tasks the user tried to solve. It must be used when user provides an answer to a task, you must provide their complete text (task and answer) as an input to this tool. "
                                          "Use the user's input text exactly letter by letter as the value for the 'input' key provided to the tool, in a JSON format representing the kwargs (e.g. {{'input': [user's input text]}}). "
                                          "Remember the output format previously mentioned to be used strictly. "
                                          "It is a must to always use this tool. "
                                          "Use only the output of Bewertung (Feedback) from this tool exactly as your output and final response. "
                                          "Copy the output from this tool letter by letter to be exactly the same as your output (Answer:).")
    )

    # Define the list of query engine tools
    query_engine_tools = [single_query_engine_tool]

    question_gen = LLMQuestionGenerator.from_defaults(
        service_context=service_context, prompt_template_str=custom_prompts.get_sub_question_query_engine_prompt()
    )

    response_synthesizer = get_response_synthesizer(text_qa_template=custom_prompts.get_text_qa_template())

    s_engine = SubQuestionQueryEngine.from_defaults(
        query_engine_tools=query_engine_tools, question_gen=question_gen, service_context=service_context, verbose=True,
        response_synthesizer=response_synthesizer
    )

    s_engine_tool = QueryEngineTool(
        query_engine=s_engine,
        metadata=ToolMetadata(name="Lecture Source",
                              description="Useful when you want to answer queries that require analyzing lecture content. "
                                          "It is a must to always use this tool. "
                                          "Use a detailed plain text question as input to the tool.")
    )

    output_parser = CustomOutputParser()

    react_chat_formatter = ReActChatFormatter(tools=[s_engine_tool, tasks_questions_query_engine_tool, tasks_answers_query_engine_tool, tasks_feedback_query_engine_tool])

    agent_chain = ReActAgent.from_tools([s_engine_tool, tasks_questions_query_engine_tool, tasks_answers_query_engine_tool, tasks_feedback_query_engine_tool], llm=llm, verbose=True, callback_manager=callback_manager,
                                        output_parser=output_parser, react_chat_formatter=react_chat_formatter)

    cl.user_session.set("chat_agent_engine", agent_chain)


# Function to handle messages in the chat
@cl.on_message
async def send_message(message):
    query_engine = cl.user_session.get("chat_agent_engine")

    while len(query_engine.chat_history) > 5:
        query_engine.chat_history.pop(0)

    response = await cl.make_async(query_engine.chat)(message + agent_input_instructions)

    costs = cl.user_session.get("costs")

    agent_cost = costs.calculate_agent_cost(query_engine, response)

    inner_steps_cost = costs.get_costs()

    print("LLM Cost: $", str(round(agent_cost + inner_steps_cost, 5)))

    response = LatexNodes2Text().latex_to_text(response.response)

    if print_cost:
        elements = [cl.Text(content="$" + str(round(agent_cost + inner_steps_cost, 5)), name="LLM Cost", display="inline")]

        # Send the response
        await cl.Message(content=response, elements=elements).send()
    else:
        # Send the response
        await cl.Message(content=response.response).send()


