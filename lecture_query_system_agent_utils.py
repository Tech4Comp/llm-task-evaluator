from llama_index.agent.react.output_parser import ReActOutputParser
from llama_index.output_parsers.utils import extract_json_str
from typing import Tuple
from llama_index.agent.react.types import (
    BaseReasoningStep,
    ResponseReasoningStep,
    ActionReasoningStep,
)
import ast

import re

def extract_tool_use(input_text: str) -> Tuple[str, str, str]:
    pattern = r"\s*Thought:(.*?)Action:(.*?)Action Input:(.*?)(?:\n|$)"

    match = re.search(pattern, input_text, re.DOTALL)
    if not match:
        raise ValueError(
            "Could not extract tool use from input text: {}".format(input_text)
        )

    thought = match.group(1).strip()
    action = match.group(2).strip()
    action_input = match.group(3).strip()
    return thought, action, action_input


def extract_final_response(input_text: str) -> Tuple[str, str]:
    pattern = r"\s*Thought:(.*?)Answer:(.*)"

    match = re.search(pattern, input_text, re.DOTALL)
    if not match:
        raise ValueError(
            "Could not extract final answer from input text: {}".format(input_text)
        )

    thought = match.group(1).strip()
    answer = match.group(2).strip()
    return thought, answer

class CustomOutputParser(ReActOutputParser):
    """ReAct Output parser."""

    def parse(self, output: str) -> BaseReasoningStep:
        """Parse output from ReAct agent.

        We expect the output to be in one of the following formats:
        1. If the agent need to use a tool to answer the question:
            ```
            Thought: <thought>
            Action: <action>
            Action Input: <action_input>
            ```
        2. If the agent can answer the question without any tools:
            ```
            Thought: <thought>
            Answer: <answer>
            ```
        """
        if "Thought:" not in output:
            # NOTE: handle the case where the agent directly outputs the answer
            # instead of following the thought-answer format
            return ResponseReasoningStep(
                thought="I can answer without any tools.", response=output
            )

        if "Answer:" in output:
            thought, answer = extract_final_response(output)
            return ResponseReasoningStep(thought=thought, response=answer)

        if "Action:" in output:
            thought, action, action_input = extract_tool_use(output)
            json_str = extract_json_str(action_input)

            # NOTE: we found that json.loads does not reliably parse
            # json with single quotes, so we use ast instead
            # action_input_dict = json.loads(json_str)
            action_input_dict = ast.literal_eval(json_str)

            return ActionReasoningStep(
                thought=thought, action=action, action_input=action_input_dict
            )

        raise ValueError("Could not parse output: {}".format(output))

    def format(self, output: str) -> str:
        """Format a query with structured output formatting instructions."""
        raise NotImplementedError


##################################################################################

# ReAct agent formatter

from abc import abstractmethod
from typing import List, Optional, Sequence

from pydantic import BaseModel

from llama_index.agent.react.types import BaseReasoningStep, ObservationReasoningStep
from llama_index.llms.base import ChatMessage, MessageRole
from llama_index.tools import BaseTool


def get_react_tool_descriptions(tools: Sequence[BaseTool]) -> List[str]:
    """Tool"""
    tool_descs = []
    for tool in tools:
        tool_desc = (
            f"> Tool Name: {tool.metadata.name}\n"
            f"Tool Description: {tool.metadata.description}\n"
            f"Tool Args: {tool.metadata.fn_schema_str}\n"
        )
        tool_descs.append(tool_desc)
    return tool_descs


# TODO: come up with better name
class BaseAgentChatFormatter(BaseModel):
    """Base chat formatter."""

    tools: Sequence[BaseTool]

    class Config:
        arbitrary_types_allowed = True

    @abstractmethod
    def format(
        self,
        chat_history: List[ChatMessage],
        current_reasoning: Optional[List[BaseReasoningStep]] = None,
    ) -> List[ChatMessage]:
        """Format chat history into list of ChatMessage."""


class ReActChatFormatter(BaseAgentChatFormatter):
    """ReAct chat formatter."""

    system_header: str = """\
You are designed to help with a variety of tasks, from answering questions \
    to providing summaries to other types of analyses.

## Tools
You have access to a wide variety of tools. You are responsible for using
the tools in any sequence you deem appropriate to complete the task at hand.
This may require breaking the task into subtasks and using different tools
to complete each subtask.

You have access to the following tools:
{tool_desc}

## Output Format
To answer the question, please use the following format.

```
Thought: I need to use a tool to help me answer the question.
Action: tool name (one of {tool_names})
Action Input: the input to the tool, in a JSON format representing the kwargs (e.g. {{"text": "hello world", "num_beams": 5}})
```
Please use a valid JSON format for the action input. Do NOT do this {{'text': 'hello world', 'num_beams': 5}}.

If this format is used, the user will respond in the following format:

```
Observation: tool response
```

You should keep repeating the above format until you have enough information
to answer the question without using any more tools. At that point, you MUST respond
in the following format:

```
Thought: I can answer without using any more tools.
Answer: [your answer here]
```

## Important Instructions
If the task cannot be related to the Math 1 course or lecture content, respond using the previous format by apologizing politely and informing that you can only answer questions about the Math 1 course. Your final answer must be a detailed answer from observation below.
Remember as an AI Tutor for the Math 1 Lecture, you don't possess any inherent knowledge. Your function is to utilize specific tools to respond to student queries. These tools are your sole source of information and knowledge, ensuring the accuracy and preciseness of your responses.
It's important to remember not to generate responses based on any perceived inherent knowledge; rely solely on the tools provided. You can't produce accurate answers on your own. Your final response must be similar if not the same as the provided observation below. Don't summarize or shorten it.
If at anytime you aren't sure whether to use the tools or not, then use the tools directly without having to ask the user about it. Remember to use the current conversation (chat history) first if it can already help you answer the task without the tools. The final response must be in German.
The Action Input for each task or subtask should be a complete, meaningful question or statement that mirrors the task you're aiming to solve. This helps provide more accurate answers. If a user's question is unrelated to the Math 1 course or lecture content, politely apologize.

## Current Conversation
Below is the current conversation consisting of interleaving human and assistant messages.

"""  # noqa: E501

    def format(
        self,
        chat_history: List[ChatMessage],
        current_reasoning: Optional[List[BaseReasoningStep]] = None,
    ) -> List[ChatMessage]:
        """Format chat history into list of ChatMessage."""
        current_reasoning = current_reasoning or []

        tool_descs_str = "\n".join(get_react_tool_descriptions(self.tools))

        fmt_sys_header = self.system_header.format(
            tool_desc=tool_descs_str,
            tool_names=", ".join([tool.metadata.get_name() for tool in self.tools]),
        )

        # format reasoning history as alternating user and assistant messages
        # where the assistant messages are thoughts and actions and the user
        # messages are observations
        reasoning_history = []
        for reasoning_step in current_reasoning:
            if isinstance(reasoning_step, ObservationReasoningStep):
                message = ChatMessage(
                    role=MessageRole.USER,
                    content=reasoning_step.get_content(),
                )
            else:
                message = ChatMessage(
                    role=MessageRole.ASSISTANT,
                    content=reasoning_step.get_content(),
                )
            reasoning_history.append(message)

        formatted_chat = [
            ChatMessage(role=MessageRole.SYSTEM, content=fmt_sys_header),
            *chat_history,
            *reasoning_history,
        ]
        return formatted_chat

agent_input_instructions = " (Remember you don't have any knowledge and you have only the tools to use at your disposal. These tools can help provide answers to user's questions accurately. You can also use the information in the current conversation (chat history) if they are sufficient to provide accurate answers. Never write anything in the output that isn't cited or quoted before in the current conversation. Your final response must be similar if not the same as the provided observation below. Don't summarize or shorten it. Let's think step by step how to solve this question or reply to the statement)\n"


##################################################################################


from llama_index.question_gen import prompts
from llama_index.prompts.base import Prompt


class CustomPrompts:
    """Custom prompts for different engines and steps."""

    # Function to get the chat prompt template
    def get_text_qa_template(self):

        DEFAULT_TEXT_QA_PROMPT_TMPL = """\
Context information and documents are below.

START VERIFIED DOCUMENTS
---------------------
{context_str}
---------------------
END VERIFIED DOCUMENTS

Given the context information and documents above, first nicely format them and deeply understand them, then answer the question in German and provide a detailed answer based only on the context information and provided documents above. The only verified documents are between "START VERIFIED DOCUMENTS and END VERIFIED DOCUMENTS". Answer solely based on these documents. Say I don't know if you are unable to answer the question based on thes documents. Never write anything in the output that isn't cited or quoted above in the documents. Let's think step by step about how to solve this question in German. Describe the question in detail before attempting to answer it in German: {query_str}\n
"""

        DEFAULT_TEXT_QA_PROMPT_CUSTOM = Prompt(
            DEFAULT_TEXT_QA_PROMPT_TMPL
        )

        return DEFAULT_TEXT_QA_PROMPT_CUSTOM

    def get_sub_question_query_engine_prompt(self):

        new_prefix = """\
Given a user question, and a list of tools, output a maximum of 3 relevant sub-questions that when composed can help answer the full user question. Only use the tools strictly from the provided list of tools <Tools>. Don't assume the subquestion to be answered from a specific lecture, e.g., (user question) in Lecture 1 or (user question) in Lecture 2, unless that's mentioned explicitly in the user question. Provide the sub-question only in German. Don't add "assistant:" to the output, and always add json markdown notation around your output. e.g. ```json(output)```:
"""

        sub_question_prompt_template = new_prefix.lstrip() + prompts.EXAMPLES.lstrip() + prompts.SUFFIX.lstrip()

        return sub_question_prompt_template


##################################################################################

import tiktoken

class Costs():
    """Functions to get costs."""
    def __init__(self, token_counter):
        # Cost definition for various model usage
        self.cost = {
                "model_prompt": 0.0015,
                "model_completion": 0.002,
                "model_embed": 0.0001,
            }

        self.token_counter = token_counter


    # Function to calculate and print the total costs
    def get_costs(self):
        total_cost = self.cost ["model_embed"] * (self.token_counter.total_embedding_token_count / 1000) + self.cost ["model_prompt"] * (
                self.token_counter.prompt_llm_token_count / 1000) + self.cost ["model_completion"] * (
                             self.token_counter.completion_llm_token_count / 1000)

        # reset the counts at your discretion!
        self.token_counter.reset_counts()

        return total_cost


    def calculate_agent_cost(self, agent_chain, response):
        encoding = tiktoken.encoding_for_model("gpt-3.5-turbo")
        model_input = []

        for chat_message in agent_chain.chat_history:
            model_input.append(chat_message.content)

        prompt_llm_token_count = len(encoding.encode(" ".join(model_input)))
        completion_llm_token_count = len(encoding.encode(response.response))

        total_cost = self.cost ["model_prompt"] * (
                prompt_llm_token_count / 1000) + self.cost ["model_completion"] * (
                             completion_llm_token_count / 1000)

        return total_cost

